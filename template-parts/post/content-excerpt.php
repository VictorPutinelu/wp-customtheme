<?php 
/**
 * Template part for displaying posts with excerpts
 * 
 * @link https://codex.wordpress.org/Template_Hierarchy 
 */
?>
<article id="post-<?php the_ID(); ?>">
	<h2>
		<a href="index.php?p=<?php the_ID(); ?>">
			<?php the_title(); ?>
		</a>
	</h2>
	<h4>
		Date: <?php the_date(); ?>
		 - 
		Author: <?php the_author(); ?>
	</h4>
	<?php the_excerpt(); ?>
</article>