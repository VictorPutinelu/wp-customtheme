<?php 
/**
 * Template part per visualizzare i post
 * 
 * @link https://codex.wordpress.org/Template_Hierarchy 
 */

?>
<article id="post-<?php the_ID(); ?>">
	<h2>
		<a href="index.php?p=<?php the_ID(); ?>">
			<?php the_title(); ?>
		</a>
	</h2>
	<div class="row">
		<div class="col-md-4">Date: <?php the_date(); ?></div>
		<div class="col-md-4">Author: <?php the_author(); ?></div>
		<div class="col-md-4">
			Category: <br>
			<?php the_category(); ?>
		</div>
	</div>
	<?php the_content(); ?>
</article>