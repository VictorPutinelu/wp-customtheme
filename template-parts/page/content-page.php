<article id="post-<?php the_ID(); ?>">
	<h1>
		<a href="index.php?p=<?php the_ID(); ?>">
			<?php the_title(); ?>
		</a>
	</h1>
	<?php the_content(); ?>
</article>