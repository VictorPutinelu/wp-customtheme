<?php
/**
 * CustomTheme's functions and definitions
 *
 * @package CustomTheme
 * @since CustomTheme 0.1
 */

if ( ! function_exists( 'customtheme_setup' ) ) {
	function customtheme_setup() {
		/**
		 * Source: https://developer.wordpress.org/themes/basics/theme-functions/#load-text-domain
		 * 
	     * Make theme available for translation.
	     * Translations can be placed in the /languages/ directory.
	     */
		// load_theme_textdomain( 'customtheme', get_template_directory() . '/languages' );

	    /**
	     * Enable support for post thumbnails and featured images.
	     */
	    // add_theme_support( 'post-thumbnails' );
 
		// Source: https://developer.wordpress.org/themes/basics/theme-functions/#navigation-menus
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'customtheme' ),
		) );

		/**
	     * Enable support for the following post formats:
	     * aside, gallery, quote, image, and video
	     */
    	// add_theme_support( 'post-formats', array ( 'aside', 'gallery', 'quote', 'image', 'video' ) );
	}

}
add_action( 'after_setup_theme', 'customtheme_setup' );

function customtheme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'customtheme' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'customtheme' ),
		'before_widget' => '<section id="%1$s" class="widget border-bottom %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h5 class="widget-title h6">',
		'after_title'   => '</h5>',
	) );
}
add_action( 'widgets_init', 'customtheme_widgets_init' );



// Source: https://developer.wordpress.org/themes/basics/including-css-javascript/
function customtheme_scripts() {
	wp_enqueue_style( 'style', get_stylesheet_uri(), false, '0.1', 'all' );

	// CSS
	// Source: https://developer.wordpress.org/reference/functions/wp_enqueue_style/

	// Check if the file exists
	// $bootstrap_css = get_stylesheet_directory() . '/css/bootstrap/css/bootstrap.min.css';
	// var_dump( file_exists( $bootstrap_css ) );
	
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap/css/bootstrap.min.css', [], 'v4.1.2', 'all' );

	// Scripts
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/css/bootstrap/js/bootstrap.min.js', ['jquery'], 'v4.1.2', true );
}

add_action( 'wp_enqueue_scripts', 'customtheme_scripts' );

// Custom Navbar
require get_template_directory() . '/inc/custom-navbar.php';