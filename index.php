<?php get_header(); ?>
	<main class="container">
		<h3 class="text-muted text-center py-5">
			<?php bloginfo( 'description' ); ?>
		</h3>
		<?php 
		// Visualizza un immagine che si trova all'interno della cartella del tema
		/*
			<img src="<?php echo get_parent_theme_file_uri( 'images/bootstrap-logo.jpg' ) ?>">
		*/
		?>
		<div class="row">
			<div class="col-lg-8">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();

					// Inclusione del template per visualizzare il post
					get_template_part( 'template-parts/post/content-excerpt', get_post_format() );
				endwhile;
			else:
				// Quando non vengono trovati post stampa il seguente messaggio
				_e( 'Sorry, no posts matched your criteria.', 'textdomain' );
			endif;
			?>
			</div>
			<div class="col-md-4">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</main>
<?php get_footer(); ?>