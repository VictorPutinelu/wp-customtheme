<?php 
/**
 * Template per visualizzare le pagine (page)
 * 
 */

get_header();
?>
<div class="container py-5">
	<?php 
	if ( have_posts() ) :
		while( have_posts() ) :
			the_post();
			get_template_part( 'template-parts/page/content', 'page' );
			
		endwhile;
	endif;
	?>
</div>
<?php get_footer(); ?>