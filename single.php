<?php 
/**
 * Template per visualizzare tutti i post singoli
 * 
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 */

get_header();
?>
<div class="container py-5">
	<div class="row">
		<div class="col-lg-8">
		<?php 
		if ( have_posts() ) :
			while( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/post/content', get_post_format() );
				
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			endwhile;
		endif;
		?>
			<div class="row">
				<div class="col-sm-6">
					<?php previous_post_link(); ?>
				</div>
				<div class="col-sm-6 text-sm-right">
					<?php next_post_link(); ?>
				</div>
			</div>
			<hr>
		</div>
		<div class="col-md-4">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>